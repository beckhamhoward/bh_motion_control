#!/usr/bin/env python2

#import roboclaw
import time
# This is the function that you want to use in order to create the matrices
# that will be used in the system.
import numpy
import socket

hostname = socket.gethostname()
if hostname == "HOWARD":
    import roboclaw
else:
    import roboclaw_mod as roboclaw



# Constants used to define the different wheel addresses
Wheel1 = 0x81
Wheel2 = 0x82
Wheel3 = 0x82
# On the roboclaw what motor is being referenced for the respective wheels
Motor1 = 1
Motor2 = 2
Motor3 = 1

# pidq constants
p = 261618
i = 126675
d = 61439
q = 44000

def readM3pidq(addr):
    return roboclaw.readM1pidq(addr)

def SetM3pidq(addr,p,i,d,qqps):
    roboclaw.SetM1pidq(addr,p,i,d,qqps)

def SetAllpidq():
    roboclaw.SetM1pidq(Wheel1,p,i,d,q)
    roboclaw.SetM2pidq(Wheel2,p,i,d,q)
    SetM3pidq(Wheel3,p,i,d,q)

def main():
    roboclaw.ResetEncoderCnts(Wheel1)
    roboclaw.ResetEncoderCnts(Wheel2)
    roboclaw.ResetEncoderCnts(Wheel3)
    #print "Wheel 1 pidq values %d %d %d %d" % roboclaw.readM1pidq(Wheel1)
    #print "Wheel 2 pidq values %d %d %d %d" % roboclaw.readM2pidq(Wheel2)
    #print "Wheel 3 pidq values %d %d %d %d" % readM3pidq(Wheel3)
    SetAllpidq()
    #print "Wheel 3 pidq values %d %d %d %d" % readM3pidq(Wheel3)
    #print "Wheel 2 pidq values %d %d %d %d" % roboclaw.readM2pidq(Wheel2)
    #print "Wheel 1 pidq values %d %d %d %d" % roboclaw.readM1pidq(Wheel1)
    while True:
        roboclaw.M1Forward(0x81, 127)
        roboclaw.M2Forward(0x82, 127)
        roboclaw.M1Forward(0x82, 127)
        time.sleep(1)
        print "M1 speed is %d" % roboclaw.readM1speed(0x81)
        print "M2 speed is %d" % roboclaw.readM2speed(0x82)
        print "M3 speed is %d" % roboclaw.readM1speed(0x82)
        roboclaw.M1Forward(0x81, 0)
        roboclaw.M2Forward(0x82, 0)
        roboclaw.M1Forward(0x82, 0)
        time.sleep(4)

        print "M1 speed is %d" % roboclaw.readM1speed(0x81)
        print "M2 speed is %d" % roboclaw.readM2speed(0x81)

if __name__  == "__main__":
    main()
