#!/usr/bin/env python2
import tty, sys, termios, threading
import rospy
from geometry_msgs.msg import Pose2D

class _Getch:
    def __init__(self):
        self.impl = _GetchUnix()
    def __call__(self):
        return self.impl()

class _GetchUnix:
    def __call__(self):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

the_key = None
cont = True

def bg():
    global the_key, cont
    while cont:
        getch = _Getch()
        the_key = getch()
        if the_key == 'q':
            break

def main():
    global cont, the_key
    rospy.init_node('tester')
    pub = rospy.Publisher('velocity', Pose2D, queue_size=1)
    t = threading.Thread(target=bg)
    t.daemon = True
    t.start()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        vel = [0, 0, 0]
        if the_key is not None:
            if the_key == '8':
                vel[1] -= 0.5
            elif the_key == '2':
                vel[1] += 0.5
            elif the_key == '4':
                vel[0] -= 0.5
            elif the_key =='6':
                vel[0] += 0.5
            elif the_key =='5':
                vel[2] += 1
            elif the_key =='0':
                vel[2] -= 1
            elif the_key == 'q':
                return
            the_key = None
        pub.publish(Pose2D(vel[0], vel[1], vel[2]))
        rate.sleep()
    cont = False
    t.join()

if __name__ == '__main__':
    main()

