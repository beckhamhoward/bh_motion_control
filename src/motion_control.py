#!/usr/bin/env python2

import math
import numpy as np
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Pose2D
from bh_vision.msg import Field
from bh_motion_control.msg import Speed
import time as clock
import socket

# Get the hostname and import appropriate version of roboclaw code.
hostname = socket.gethostname()
if hostname == "HOWARD":
    import roboclaw as rc
else:
    import roboclaw_mod as rc

# value used to turn on all debug values
PRINT_DEBUG = False

## Define constants to use for the program
# measurments made in inches
body_radius_in = 3.1
wheel_dia_in_in = 2
wheel_dia_out_in = 2.29
wheel_circum_in = 7;
# Conversion factor
convert_in_m = 2.45/100
# Converted measurements
body_radius_m = body_radius_in*convert_in_m
wheel_dia_in_m = wheel_dia_in_in*convert_in_m
wheel_dia_out_m = wheel_dia_out_in*convert_in_m
wheel_circum_m = wheel_circum_in*convert_in_m
# Solve for the radius of the wheels
wheel_radius_m = wheel_circum_m/(math.pi*2)
# angle from the front of the robot to the respective wheel
angle1_deg = -60
angle2_deg = 60
angle3_deg = 180
# Convert degrees to angles
convert_deg_rad = math.pi/180
# Converted angles
angle1_rad = angle1_deg*convert_deg_rad
angle2_rad = angle2_deg*convert_deg_rad
angle3_rad = angle3_deg*convert_deg_rad
# Define a unit vector length and angles
unit_vector_length = 1
s1_deg = -90 + angle1_deg
s2_deg = angle2_deg + 90
s3_deg = angle3_deg - 90
s1_rad = s1_deg*convert_deg_rad
s2_rad = s2_deg*convert_deg_rad
s3_rad = s3_deg*convert_deg_rad
# Define a constant to convert from radians to ticks per second
convert_rad_ticks = 19822/(2*math.pi)

# Set the conter total for debugging functions
COUNTER_TOTAL = 50

# Set the addresses for each of the motors
ADDRESS_1 = 0x81
ADDRESS_2 = 0x82
ADDRESS_3 = 0x82

# Define the local M matrix variable
#M = np.matrix([[0, 0, 0], [0, 0, 0], [0, 0, 0]])

# Define the local R matrix variable
#R = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])

# Function to write to the motors
def setMotorSpeeds(speed1, speed2, speed3, debug):
    rc.SetM1Speed(ADDRESS_1, speed1)
    rc.SetM2Speed(ADDRESS_2, speed2)
    rc.SetM1Speed(ADDRESS_3, speed3)
    if debug:
        print("Speeds have been set")

# Function to read back the speeds of each wheel
#def readMotorSpeeds(debug):
    #Omega = [[rc.readM1speed(ADDRESS_1)], [rc.readM2speed(ADDRESS_2)], [rc.readM1speed(ADDRESS_3_]]

class Node():
    def __init__(self):
        self.__setup_M(PRINT_DEBUG)
        # initial command: No command
        self.velocity = None
        # initial theta: 0
        self.theta = 0
        # Nodes are uniquely named.  Don't have names that are the same that
        # Will cause lots of problems.
        rospy.init_node('motion_control', anonymous=False)
        # outline of the how to set up a subscriber to listen for comands
        # rospy.Subscriber("test", String, callback)
        # Listen to the AI commands
        rospy.Subscriber("velocity", Pose2D, self.velocity_callback)
        # Listen to the field vision data
        rospy.Subscriber("positions_est", Field, self.position_callback)
        # Setup the publisher to send information the the Estimator
        pub = rospy.Publisher('deadReckon_vel', Speed, queue_size=10)
        # set up a tick rate
        rate = rospy.Rate(20)
        # loop infinitely until the node receives shutdown (either from Ctrl-C
        # or some other ros source
        print "Startup complete, performing velocity reads"
        while not rospy.is_shutdown():
            # perform movement if needed
            velocity = self.velocity
            self.velocity = None # make sure we execute moveRobot once per velocity command
            if velocity is not None:
                # run the Move function based on the given position, x, y, and theta
                # also trigger the print statements for debuging purposes
                try:
                    moveRobot([0, 0, self.theta], [[velocity.x], [velocity.y], [velocity.theta]], PRINT_DEBUG)
                except Exception as e:
                    print "Error moving robot", str(e)
            # perform dead reckoning and publish
            try:
                vel_out = deadReckoning(self.theta, PRINT_DEBUG)
                if vel_out is not None:
                    VelPassThrough = Pose2D(vel_out[0], vel_out[1], vel_out[2])
                    pub.publish(Speed(clock.time(), VelPassThrough))
            except Exception as e:
                print "Error dead reckoning", str(e)
            # sleep to get the tick rate as set up above
            rate.sleep()

        rospy.spin()

    def __setup_M(self, debug):
        # Q used to be 44000 I reduced the charge to make it slower to avoidn
        # overheating.
        rc.SetM1pidq(0x81, 261618, 126675, 61439, 138754)
        rc.SetM2pidq(0x82, 261618, 126675, 61439, 138754)
        rc.SetM1pidq(0x82, 261618, 126675, 61439, 138754)
        # Set the position vectors of the wheels based on the
        # constants defined above.
        r1 = setVectors(body_radius_m, angle1_rad, debug)
        r2 = setVectors(body_radius_m, angle2_rad, debug)
        r3 = setVectors(body_radius_m, angle3_rad, debug)
        # Set the unit vectors for the wheels based on the constants
        # defined above
        s1 = setVectors(unit_vector_length, s1_rad, debug)
        s2 = setVectors(unit_vector_length, s2_rad, debug)
        s3 = setVectors(unit_vector_length, s3_rad, debug)
        # Create the matrix based on the documentation given by Dr.
        # Beard in class.  This will rotate from the body frame to
        # the frame of the wheels
        m = np.matrix([[s1[0], s1[1], (s1[1]*r1[0]-s1[0]*r1[1])],
                [s2[0], s2[1], (s2[1]*r2[0]-s2[0]*r2[1])],
                [s3[0], s3[1], (s3[1]*r3[0]-s3[0]*r3[1])]])
        # Solve for the final matrix
        M = 1/wheel_radius_m*m
        # If debug is turned on print out the relevant information
        # along with a header to identify the information in the 
        # terminal
        if debug:
            print "M is..."
            print M
        self.M = M
        
    ##########################################################################
    ##########################################################################
    # Fix me
    ##########################################################################
    ##########################################################################
    def position_callback(self, data):
        if data.hasHome2: # this will need to be dynamic (do we look at home1? away2? etc)
            self.theta = data.home2.theta

    def velocity_callback(self, data):
        # Print a new header to know what type of information is
        # being entered into the terminal
        print "New data incoming from AI"
        # Print the relevant data
        x = data.x
        y = data.y
        theta = data.theta
        print "incoming data"
        print data.x
        print data.y
        print data.theta
        self.velocity = data

# Function to set the position vectors for the body this is used
# to define the r and s vectors for each of the wheels. The 
# debug is a global variable that will be used to determine 
# print statements in order to see what is happening at each
# stage of the build functions
def setVectors(radius, angle, debug):
    r1 = radius*math.cos(angle)
    r2 = radius*math.sin(angle)
    # Assuming that the third element is never callled set the#
    # angle use to calculate the postion to this element
    r3 = angle
    # Convert the angle back to degrees for print statements
    #if debug:
        # Convert the angle back to degrees for print statements
    #    angle = angle/convert_deg_rad
        # Print out a header
    #    print "for angle: %d" % angle
        # Print out the valid stats
    #    print r1#
    #    print r2
    # return the values in a numpy array
    return np.array([r1, r2, r3])

# Rotational matrix that will rotate from the body frame to the wheel frame
def bodyToAngularMatrix(debug):
    # Q used to be 44000 I reduced the charge to make it slower to avoid
    # overheating.
    rc.SetM1pidq(0x81, 261618, 126675, 61439, 138754)
    rc.SetM2pidq(0x82, 261618, 126675, 61439, 138754)
    rc.SetM1pidq(0x82, 261618, 126675, 61439, 138754)
    # Set the position vectors of the wheels based on the
    # constants defined above.
    r1 = setVectors(body_radius_m, angle1_rad, debug)
    r2 = setVectors(body_radius_m, angle2_rad, debug)
    r3 = setVectors(body_radius_m, angle3_rad, debug)
    # Set the unit vectors for the wheels based on the constants
    # defined above
    s1 = setVectors(unit_vector_length, s1_rad, debug)
    s2 = setVectors(unit_vector_length, s2_rad, debug)
    s3 = setVectors(unit_vector_length, s3_rad, debug)
    # Create the matrix based on the documentation given by Dr.
    # Beard in class.  This will rotate from the body frame to
    # the frame of the wheels
    m = np.matrix([[s1[0], s1[1], (s1[1]*r1[0]-s1[0]*r1[1])],
            [s2[0], s2[1], (s2[1]*r2[0]-s2[0]*r2[1])],
            [s3[0], s3[1], (s3[1]*r3[0]-s3[0]*r3[1])]])
    # Solve for the final matrix
    M = 1/wheel_radius_m*m
    # If debug is turned on print out the relevant information
    # along with a header to identify the information in the 
    # terminal
    if debug:
        print "M is..."
        print M
    return M

# Rotational matrix that will rotate from the world frame to 
# the body frame
def worldToBodyMatrix(debug, angle):
    # Assuming that theta is given to the controler in degrees
    theta = angle
    # Solve for the rotational matrix as defined by Dr. Beard
    # in class and on the wiki
    r = np.matrix([[math.cos(-theta), -math.sin(-theta), 0],
            [math.sin(-theta), math.cos(-theta), 0],
            [0, 0, 1]])
    T = np.matrix([[1, 0, 0],[0,-1,0],[0,0,1]])
    R = T*(r)
    # If debug is turned on print the R matrix with a header for
    # identification
    if debug:
        print "The R matrix is..."
        print R
    # Return the Rotational matrix to the calling function
    return R

# Define a function that will generate movement based on commands given
# inside this file rather than recieving commands from an AI.  Pass into the
# function an array of size three and a debug value to determine if the
# function should print debug statements
def moveRobot(posStart, vel, debug):
    # get the angle from the last element in the array
    theta = posStart[2]
    # call the bodyToAngularMatrix function once to define the M matrix
    M = bodyToAngularMatrix(debug)
    # Always get the new R matrix based on the position theta from the
    # camera
    R = worldToBodyMatrix(debug, theta)
    # convert vel to Omega (defines the wheel movement)
    temp = M*R
    omega = temp.dot(vel)
    omega = convert_rad_ticks*omega
    Omega = np.array([int(omega[0]), int(omega[1]), int(omega[2])])
    # make the motors move according to omega
    rc.SetM1Speed(0x81, (Omega[0]))
    rc.SetM2Speed(0x82, (Omega[1]))
    rc.SetM1Speed(0x82, (Omega[2]))
    return clock.time()

# This function is used to communicate with the state esitimator to determine
# the position of the robot.  This is very similar to the deadReckoningLocal
# function except it will return a velocity rather than position
def deadReckoning(theta, debug):
    # set M
    M = bodyToAngularMatrix(debug)
    # set R
    R = worldToBodyMatrix(debug, theta)
    # Read in the speeds for each of the wheels and save them to Omega
    Omega1 = rc.readM1speed(0x81)
    Omega2 = rc.readM2speed(0x82)
    Omega3 = rc.readM1speed(0x82)
    if Omega1 is None or Omega2 is None or Omega3 is None:
        return None
    # Convert the ticks to radians
    Omega1 = Omega1/convert_rad_ticks
    Omega2 = Omega2/convert_rad_ticks
    Omega3 = Omega3/convert_rad_ticks
    # Save the Omega# values to Omega
    Omega = np.array([[Omega1],[Omega2],[Omega3]])
    #if debug:
    #    print "Here is Omega..."
    #    print Omega
    # Reverse the process to find the v_x, v_y, and v_theta
    R_inv = np.linalg.inv(R)
    #if debug:
    #    print "Here is R..."
    #    print R
    #    print "Here is R_Trans..."
    #    print R_Trans
    M_inv = np.linalg.inv(M)
    temp = R_inv*(M_inv)
    vel = temp*(Omega)
    return vel


# This function is used to determine the position of the robot without
# any input from the ai or the vision commands.  When the robot starts
# up the program assumes that the position start at x:0, y:0, and
# theta:0.  This can be changed in the future to initially check with
# the vision thread before running a configuration pattern
def deadReckoningLocal(time, position, debug):
    # get the current time
    current_time = clock.time()
    # set M
    M = bodyToAngularMatrix(debug)
    # set R
    R = worldToBodyMatrix(debug, position[2])
    # Read in the speeds for each of the wheels and save them to the
    # respective Omega# values
    Omega1 = rc.readM1speed(0x81)
    Omega2 = rc.readM2speed(0x82)
    Omega3 = rc.readM1speed(0x82)
    # Convert the ticks to radians
    Omega1 = Omega1/convert_rad_ticks
    Omega2 = Omega2/convert_rad_ticks
    Omega3 = Omega3/convert_rad_ticks
    # Save the Omega# values to Omega
    Omega = np.array([[Omega1],[Omega2],[Omega3]])
    #if debug:
    #    print "Here is Omega..."
    #    print Omega
    # Reverse the process to find the v_x, v_y, and v_theta
    R_inv = np.linalg.inv(R)
    #if debug:
    #    print "Here is R..."
    #    print R
    #    print "Here is R_Trans..."
    #    print R_Trans
    M_inv = np.linalg.inv(M)
    #M_prod = M_inv*M
    #print M_prod
    #if debug:
    #    print "here is M..."
    #    print M
    #    print "here is M_inv..."
    #    print M_inv
    temp = R_inv*(M_inv)
    vel = temp*(Omega)
    print time
    print current_time
    current_position = vel*(current_time - time)
    if not debug:
        print "here is the actual velocity in the field frame"
        print vel
        print "here is the current position" 
        print current_position

def moveRobotDebug(position, counter_total, debug):
    # Set a counter to zero to run a function
    counter = 0
    # Define the velocities needed to run the test
    vel = np.array([[0.0], [0.5], [0.0]])
    time_start = clock.time()
    print "time_start: ", time_start
    # Define a while loop to run through a counter for testing
    while counter < counter_total:
        # Call the moveRobot function
        moveRobot(position, vel, debug)
        # Increment the counter
        counter = counter +1
    # determine where the robot is by calling the deadReckoning
    Pos = deadReckoning(time_start, position, debug)
    time_start = clock.time()
    # Create a delay to analyze the robot and give it a chance to change
    # directon
    print("Start delay")
    # turn off the motors
    vel = np.array([[0.0], [0.0], [0.0]])
    moveRobot(position, vel, debug)
    # Put the function to sleep
    rospy.sleep(10)
    # reset the counter to run another command
    counter = 0
    # define a new command to run
    #vel = np.array([[0.0], [0.5], [0.0]])
    # run the new command
    while counter < counter_total:
        # Call the moveRobot function
        moveRobot(position, vel, debug)
        # Increment the counter
        counter = counter +1
        # determine where the robot is by calling the deadReckoning
        #Pos = deadReckoning(M, R, debug, time)
    # reset the counter to run another command
    counter = 0
    # define a new command to run
    #vel = np.array([[0.0], [0.0], [10.0]])
    # run the new command
    while counter < counter_total:
        # Call the moveRobot function
        moveRobot(position, vel, debug)
        # Increment the counter
        counter = counter + 1
        # determine where the robot is by calling the deadReckoning
        #Pos = deadReckoning(M, R, debug, time)
    # at the end of the test turn off the robot
    rc.SetM1Speed(0x81, 0)
    rc.SetM2Speed(0x82, 0)
    rc.SetM1Speed(0x82, 0)
    global current_pos
    # Why doesn't this work when I try to use temp = 1/2
    temp = 0.5
    current_pos = np.array([0.0, 0.0, temp])

if __name__=="__main__":
    try:
        Node()
    except rospy.ROSInterruptException:
        pass
    #position = np.array([0, 0, -.7])
    #moveRobotDebug(position, COUNTER_TOTAL, not PRINT_DEBUG)
