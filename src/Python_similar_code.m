clear all;
close all;

%% Define the body in the measured units (inches)
body_radius_in = 3.1;
wheel_dia_in   = 2;
%% Define a conversion factor from inches to meters
convert_in_m   = 2.45/100;
%% Convert the body measurements to meters
body_radius_m  = body_radius_in*convert_in_m;
wheel_dia_m    = wheel_dia_in*convert_in_m;
%% Record the angle from the front of the robot to the wheels
angle1_deg = -60;
angle2_deg = 60;
angle3_deg = 180;
%% Define a conversion factor from degrees to radians
convert_deg_rad = pi/180;
%% Convert the angles to radians
angle1_rad = angle1_deg*convert_deg_rad;
angle2_rad = angle2_deg*convert_deg_rad;
angle3_rad = angle3_deg*convert_deg_rad;
%% Set the position vectors of each wheel[x,y,theta]
r1 = [body_radius_m*cos(angle1_rad), body_radius_m*sin(angle1_rad),...
    angle1_rad];
r2 = [body_radius_m*cos(angle2_rad), body_radius_m*sin(angle2_rad),...
    angle2_rad];
r3 = [body_radius_m*cos(angle3_rad), body_radius_m*sin(angle3_rad),...
    angle3_rad];
%% Set the direction of the unit vectors that point in the direction that
%  the wheels move on the robot
s1_deg = -90 + angle1_deg;       % should be -150
s2_deg = angle2_deg+90;          % should be 150
s3_deg = angle3_deg-90;          % should be 90
%% Convert the unit vector angles to radians from degrees
s1_rad = s1_deg*convert_deg_rad;
s2_rad = s2_deg*convert_deg_rad;
s3_rad = s3_deg*convert_deg_rad;
%% Set the movement unit vectors of each wheel [x,y,theta]
s1 = [cos(s1_rad), sin(s1_rad), s1_rad];
s2 = [cos(s2_rad), sin(s2_rad), s2_rad];
s3 = [cos(s3_rad), sin(s3_rad), s3_rad];
%% Define a matrix M to rotate from the body frame the the speed of each
%  of the wheels
m = [s1(1), s1(2), (s1(2)*r1(1)-s1(1)*r1(2));
     s2(1), s2(2), (s2(2)*r2(1)-s2(1)*r2(2));
     s3(1), s3(2), (s3(2)*r3(1)-s3(1)*r3(2))];
Rad = wheel_dia_m/2;
M = 1/Rad.*m;
%% Test the functionality of the determining Omega and OMEGA
% Set the starting position and velocity
pos = [0;0;0];
vel = [0;-0.5;0]
% Find the Rotational matrix
R = [cos(pos(3)), -sin(pos(3)), 0; sin(pos(3)), cos(pos(3)), 0; 0, 0, 1];
% Find Omega where we do all of the math operations all at the same time
Omega = M*R*vel
% Fine OMEGA where I break it down into two steps at a time
temp = M*R;
OMEGA = temp*vel;
%% Set the output to 1 if the matrices match else output 0
if OMEGA == Omega
    1;
else
    0;
end

%% Note if I did R*vel first or M*R first I got the same result.  I may
%  need to test this further with arbitrary speeds to move in the proper
%  directions.
vel_after = R'*inv(M)*Omega;
inv(M)*M
