#!/usr/bin/env python2

import rospy
from std_msgs.msg import Float64
import os, time
import socket
PRINT_DEBUG = False
class Node():
    def __init__(self):
        # Set the GPIO number here.
        hostname = socket.gethostname()
        if hostname == "HOWARD":
            self.gpio_number = 199
        else:
            self.gpio_number = 21
        rospy.init_node('kicker_control', anonymous=False)
        rospy.Subscriber('kicker', Float64, self.kicker_callback )
        rate = rospy.Rate(20)
        self.kick_time = None
        if not os.path.isdir("/sys/class/gpio/gpio" + str(self.gpio_number)):
            os.system("echo " + str(self.gpio_number) + " > /sys/class/gpio/export")
        os.system("echo out > /sys/class/gpio/gpio" + str(self.gpio_number) + "/direction") 
        while not rospy.is_shutdown():
            # do the kick
            if self.kick_time is not None:
                # Set kick time in local variable
                do_kicktime = self.kick_time
                # Reset the class's kick time so that kicks don't repeat.
                self.kick_time = None
                # kick for that amount of time
                # write 1 (enable) to file
                os.system("echo 1 > /sys/class/gpio/gpio" + str(self.gpio_number) + "/value")
                # sleep for time
                time.sleep(do_kicktime)
                # write 0 (disable) to file
                os.system("echo 0 > /sys/class/gpio/gpio" + str(self.gpio_number) + "/value")
            rate.sleep()

    def kicker_callback(self, data):
        # Set kick time
        self.kick_time = data.data

if __name__=="__main__":
    try:
        Node()
    except rospy.ROSInterruptException:
        pass
