% Define constants to use for the program
% measurments made in inches
body_radius_in = 3.1;
wheel_dia_in_in = 2;
wheel_dia_out_in = 2.29;
wheel_circum_in = 7;
% Conversion factor
convert_in_m = 2.45/100;
% Converted measurements
body_radius_m = body_radius_in*convert_in_m;
wheel_dia_in_m = wheel_dia_in_in*convert_in_m;
wheel_dia_out_m = wheel_dia_out_in*convert_in_m;
wheel_circum_m = wheel_circum_in*convert_in_m;
% Solve for the radius of the wheels
wheel_radius_m = wheel_circum_m/(math.pi*2);
% angle from the front of the robot to the respective wheel
angle1_deg = -60;
angle2_deg = 60;
angle3_deg = 180;
% Convert degrees to angles
convert_deg_rad = math.pi/180;
% Converted angles
angle1_rad = angle1_deg*convert_deg_rad;
angle2_rad = angle2_deg*convert_deg_rad;
angle3_rad = angle3_deg*convert_deg_rad;
% Define a unit vector length and angles
unit_vector_length = 1;
s1_deg = -90 + angle1_deg;
s2_deg = angle2_deg + 90;
s3_deg = angle3_deg - 90;
s1_rad = s1_deg*convert_deg_rad;
s2_rad = s2_deg*convert_deg_rad;
s3_rad = s3_deg*convert_deg_rad;
% Define a constant to convert from radians to ticks per second
convert_rad_ticks = 19822/(2*math.pi);


