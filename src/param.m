%% General parameters of the location and placement of the wheels

% Location of the wheels based on the front of the robot
theta1 = 60;	% degrees
theta2 = -60;	% degrees
theta3 = 180;	% degrees

% Distance of the wheels from the center of the robot
L1 = 3.1; % inches
L2 = 3.1; % inches
L3 = 3.1; % inches

wheel_dia_in  = 2;    % inches
wheel_dia_out = 3.29; % inches
wheel_r_in   = wheel_dia_in/2;  % inches
wheel_r_out  = wheel_dia_out/2; % inches
wheel_c_in   = pi*wheel_dia_in;  % inches
wheel_c_out  = pi*wheel_dia_out; % inches

% assume that all three wheels move the robot counterclockwise

%% General parameters of the body of the robot
